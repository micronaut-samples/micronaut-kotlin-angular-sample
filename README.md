# micronaut-kotlin-angular-sample

Sample application with Micronaut, Kotlin, Gradle and Angular
As IDE i use IntelliJ.

First setup a project `micronaut-kotlin-angular-sample` within the IDE.

#Setup micronaut app

Next step is to create a micronaut app. You could use eighter Micronaut CLI or the new starter https://micronaut.io/launch/
which is only for Version 2 but could be easily tweaked down to the last stable release.

Now set it up like that
![Starter setup](screenshots/starter.PNG)

Then press generate project and copy the content into your ide project.

Now import the build.gradle.

![Import](screenshots/import.PNG)

Now your project should look like:

![Project](screenshots/project-micronaut.PNG)

Next step is to roll back to the last stable release, in that case its 1.3.5.
Modify gradle.properties to `micronautVersion=1.3.5` and load the gradle changes.

Next step is to tweak our application.yml for a moment.

```yaml
micronaut:
  application:
    name: spaSample
  server:
    port: 8080
  security:
    enabled: false
    endpoints:
      login:
        enabled: false
      oauth:
        enabled: false
```

As you could see security is currently of. We get back on that for a moment.

To avoid some warnings we also modify the src/main/resources/logback.xml and remove ``<withJansi>true</withJansi>``

````xml
<configuration>

    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
    </appender>

    <root level="info">
        <appender-ref ref="STDOUT" />
    </root>
</configuration>

````

Now lets start the application. Output should be like that:

````
"C:\Program Files\AdoptOpenJDK\jdk-11.0.6.10-hotspot\bin\java.exe" -javaagent:C:\Users\Sambalmueslie\AppData\Local\JetBrains\Toolbox\apps\IDEA-U\ch-0\201.7223.91\lib\idea_rt.jar=54934:C:\Users\Sambalmueslie\AppData\Local\JetBrains\Toolbox\apps\IDEA-U\ch-0\201.7223.91\bin -Dfile.encoding=UTF-8 -classpath C:\Users\Sambalmueslie\IdeaProjects\micronaut-sample\micronaut-kotlin-angular-sample\build\classes\kotlin\main;C:\Users\Sambalmueslie\IdeaProjects\micronaut-sample\micronaut-kotlin-angular-sample\build\tmp\kapt3\classes\main;C:\Users\Sambalmueslie\IdeaProjects\micronaut-sample\micronaut-kotlin-angular-sample\build\resources\main;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-http-client\1.3.5\b1c19cf05ef48196ab1e57ee552f3de6b1039de7\micronaut-http-client-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-http-server-netty\1.3.5\a7e27c6856a0316f40eba244abc4b9c1014d5392\micronaut-http-server-netty-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-security-jwt\1.4.0\a406cfd30320af224976ed39521740bb198865b\micronaut-security-jwt-1.4.0.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut.configuration\micronaut-security-oauth2\1.4.0\4f0cd6446583651e71f8421d5cd62824d729bd1b\micronaut-security-oauth2-1.4.0.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-runtime\1.3.5\d45efc773cc4658f6e7e25b01551ab352ab9223f\micronaut-runtime-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-validation\1.3.5\234b42ff056ee9d9d535ed4f1332cf386cf0a235\micronaut-validation-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-inject\1.3.5\8f6a0c8de1b31870e07450222b88cfc2c6667952\micronaut-inject-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.jetbrains.kotlin\kotlin-stdlib-jdk8\1.3.72\916d54b9eb6442b615e6f1488978f551c0674720\kotlin-stdlib-jdk8-1.3.72.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.jetbrains.kotlin\kotlin-reflect\1.3.72\86613e1a669a701b0c660bfd2af4f82a7ae11fca\kotlin-reflect-1.3.72.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\javax.annotation\javax.annotation-api\1.3.2\934c04d3cfef185a8008e7bf34331b79730a9d43\javax.annotation-api-1.3.2.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-http-netty\1.3.5\d27d155e7170fcf0692602b4b2624fce4b032764\micronaut-http-netty-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-websocket\1.3.5\3a5a420bde343c4830a3679a24560fb33bde6ed8\micronaut-websocket-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-handler-proxy\4.1.48.Final\79910203db6a542189c9aaa9ec187bf5ee16248a\netty-handler-proxy-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.slf4j\slf4j-api\1.7.26\77100a62c2e6f04b53977b9f541044d7d722693d\slf4j-api-1.7.26.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.reactivex.rxjava2\rxjava\2.2.10\18e9edc67e0abaa03713eeb9ca2cb0e30c859de4\rxjava-2.2.10.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-http-server\1.3.5\69bbf7044c8e8fe6045d0ab27421004563e0f174\micronaut-http-server-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-core\1.3.5\b9ee047be587cf72f2dd9bb15539e4999c0a5758\micronaut-core-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-codec-http\4.1.48.Final\8b18499a51367d291efd1f200a4e865370d38738\netty-codec-http-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-security\1.4.0\d3fa122ba1d41ba4034fddd881b2a2d332d69c50\micronaut-security-1.4.0.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-http\1.3.5\7bc1852494f4eae298ae570778a17b80e33e055\micronaut-http-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.nimbusds\nimbus-jose-jwt\8.6\93ae6d9f03a4160e5c3ca7d0c9e6b88efbfa26e7\nimbus-jose-jwt-8.6.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-aop\1.3.5\67933d8c4dd756e0b3ce96db094e8f331203fdcb\micronaut-aop-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.datatype\jackson-datatype-jdk8\2.10.3\5fc73a94e771eb044e45e2e3ec9a38660daee6a\jackson-datatype-jdk8-2.10.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.datatype\jackson-datatype-jsr310\2.10.3\df773a823e37013779fa247cd13686ff5ee573e3\jackson-datatype-jsr310-2.10.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.core\jackson-databind\2.10.1\18eee15ffc662d27538d5b6ee84e4c92c0a9d03e\jackson-databind-2.10.1.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\javax.validation\validation-api\2.0.1.Final\cb855558e6271b1b32e716d24cb85c7f583ce09e\validation-api-2.0.1.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.yaml\snakeyaml\1.26\a78a8747147d2c5807683e76ec2b633e95c14fe9\snakeyaml-1.26.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\javax.inject\javax.inject\1\6975da39a7040257bd51d21a231b76c915872d38\javax.inject-1.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.jetbrains.kotlin\kotlin-stdlib-jdk7\1.3.72\3adfc2f4ea4243e01204be8081fe63bde6b12815\kotlin-stdlib-jdk7-1.3.72.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.jetbrains.kotlin\kotlin-stdlib\1.3.72\8032138f12c0180bc4e51fe139d4c52b46db6109\kotlin-stdlib-1.3.72.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-buffer-netty\1.3.5\5f2dd2e91cc077b1ac384da776ca75799f9ec92b\micronaut-buffer-netty-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-handler\4.1.48.Final\d459919c87dc1a2bf400b39053761ecc11c2e436\netty-handler-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-codec-socks\4.1.48.Final\42b55c9ca8672ebb7160cadf3c70ffc11638f021\netty-codec-socks-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-codec\4.1.48.Final\3142078325d745228da9d6d1f6f9931c63aaba16\netty-codec-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport\4.1.48.Final\d285e8d9af5be1fc146b91a1b0d211e7a6a7e7d4\netty-transport-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-buffer\4.1.48.Final\7fd3ebb659c628e0158c9c2c971f4d0734d7f4f0\netty-buffer-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-common\4.1.48.Final\ec27d04fd5e7b65ac171cd64734f9e4f1fc2a286\netty-common-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.reactivestreams\reactive-streams\1.0.3\d9fb7a7926ffa635b3dcaa5049fb2bfa25b3e7d0\reactive-streams-1.0.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-router\1.3.5\7ba4b6cff28110e520bc6a071a76a27ea0b9bb85\micronaut-router-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.google.code.findbugs\jsr305\3.0.2\25ea2e8b0c338a877313bd4672d3fe056ea78f0d\jsr305-3.0.2.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-management\1.3.5\e67c0c355f2da01c23e5645d1a8c1189120c6fb6\micronaut-management-1.3.5.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.micronaut\micronaut-security-annotations\1.4.0\eb2b5b459711e46ea4dde234f4bbb2b7578a7954\micronaut-security-annotations-1.4.0.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.github.stephenc.jcip\jcip-annotations\1.0-1\ef31541dd28ae2cefdd17c7ebf352d93e9058c63\jcip-annotations-1.0-1.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\net.minidev\json-smart\2.3\7396407491352ce4fa30de92efb158adb76b5b\json-smart-2.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.core\jackson-core\2.10.3\f7ee7b55c7d292ac72fbaa7648c089f069c938d2\jackson-core-2.10.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.core\jackson-annotations\2.10.3\f63b3b1da563767d04d2e4d3fc1ae0cdeffebe7\jackson-annotations-2.10.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.jetbrains.kotlin\kotlin-stdlib-common\1.3.72\6ca8bee3d88957eaaaef077c41c908c9940492d8\kotlin-stdlib-common-1.3.72.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.jetbrains\annotations\13.0\919f0dfe192fb4e063e7dacadee7f8bb9a2672a9\annotations-13.0.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\io.netty\netty-resolver\4.1.48.Final\612ad2acc848ce46496095683eea621094763b80\netty-resolver-4.1.48.Final.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\net.minidev\accessors-smart\1.2\c592b500269bfde36096641b01238a8350f8aa31\accessors-smart-1.2.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\org.ow2.asm\asm\5.0.4\da08b8cce7bbf903602a25a3a163ae252435795\asm-5.0.4.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.module\jackson-module-kotlin\2.10.3\d6908a6c5a0ceef2b907eeae9a31fdcdd534fb19\jackson-module-kotlin-2.10.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\ch.qos.logback\logback-classic\1.2.3\7c4f3c474fb2c041d8028740440937705ebb473a\logback-classic-1.2.3.jar;C:\Users\Sambalmueslie\.gradle\caches\modules-2\files-2.1\ch.qos.logback\logback-core\1.2.3\864344400c3d4d92dfeb0a305dc87d953677c03c\logback-core-1.2.3.jar com.example.Application
15:48:19.228 [main] INFO  io.micronaut.runtime.Micronaut - Startup completed in 1894ms. Server Running: http://localhost:8080
````

#Setup angular app

Next step will be to add our angular application.
First create a new module named *angular* within your ide.

I prefer to have everything together in one place, that's why I move stuff together.
Therefore, add a directory ``src/main/webapp`` and move everything from ``angular/src`` into webapp.
Also, move everything from *angular* to project root.
Next modify the *angular.json* and replace all ``src/`` with ``src/main/webapp``.
Do the same for ``tsconfig.app.json`` and ``tsconfig.spec.json``
Move the ``e2e`` directory also to project root. Merge the .gitignore with the existing one and finally remove the module ``angular``

Run then ``npm install`` to install the project dependencies. 

Now your angular app is ready, and you could start it. After the application has started you could access it on http://localhost:4200
now you see the angular welcome screen.
![Welcome](screenshots/angular-welcome.PNG)
You could leave the app running, cause angular detect changes and reload the page.

Next will be to setup the angular app. Therefore we add material to the app (https://material.angular.io/)
Run in the webapp directory
````
ng add @angular/material
````
Select "Custom Theme" and leave everything else on default.
Run ``npm -install again`` and restart the app ``ng serve``

## Add some content
Now let's add some content to our app. We assume that we like to have a sidenav (https://material.angular.io/guide/schematics#navigation-schematic)
and two modules which are belongs to the menu.


### The navigation 

```
cd app
ng generate module nav
cd nav
ng generate @angular/material:navigation MainNav
```

Open then the app.component.html and set it to 
````html
<app-main-nav></app-main-nav>
````

Open ``main-nav.component.html`` and set the toolbar title to ``My Angular Micronaut Sample app`` 

### The material module
We put all our material imports into a separate module, to make it more easy to handle.
```
cd app
ng generate module material
```
Refactor the ``material.module.ts`` to

```typescript
const MAT_MODULES = [
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule
];

@NgModule({
  declarations: [],
  imports: MAT_MODULES,
  exports: MAT_MODULES
})
export class MaterialModule {
}
```

Set then the ``nav.module.ts`` to

````typescript

@NgModule({
  declarations: [MainNavComponent],
  exports: [
    MainNavComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule
  ]
})
export class NavModule {
}
````
Now your page should look like:
![App](screenshots/app-01.PNG)

Next will be to add some routing module to main.
### The main routing module
For more details on routing see (https://angular.io/guide/router)

Cause the routing module has not been created during setup, we have to do it manually.
(https://stackoverflow.com/questions/44990030/how-to-add-a-routing-module-to-an-existing-module-in-angular-cli-version-1-1-1)

We put our main navigation into the nav module, so the main routing config should be also there.
Create a file ``nav-routing.module.ts`` with that content
````typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NavRoutingModule { }
````
and modify the main nav component to
````html
<mat-sidenav-container class="sidenav-container">
  <mat-sidenav #drawer class="sidenav" fixedInViewport
               [attr.role]="(isHandset$ | async) ? 'dialog' : 'navigation'"
               [mode]="(isHandset$ | async) ? 'over' : 'side'"
               [opened]="(isHandset$ | async) === false">
    <mat-toolbar>Menu</mat-toolbar>
    <mat-nav-list>
      <a mat-list-item href="#">Link 1</a>
      <a mat-list-item href="#">Link 2</a>
      <a mat-list-item href="#">Link 3</a>
    </mat-nav-list>
  </mat-sidenav>
  <mat-sidenav-content>
    <mat-toolbar color="primary">
      <button
        type="button"
        aria-label="Toggle sidenav"
        mat-icon-button
        (click)="drawer.toggle()"
        *ngIf="isHandset$ | async">
        <mat-icon aria-label="Side nav toggle icon">menu</mat-icon>
      </button>
      <span>My Angular Micronaut Sample app</span>
    </mat-toolbar>
    <router-outlet></router-outlet> # add that line
  </mat-sidenav-content>
</mat-sidenav-container>
````

Now we are ready to do the routing. Next will be to add the modules and do some lazy loading on them.







# Links
https://micronaut.io/launch/
https://guides.micronaut.io/micronaut-spa-react/guide/index.html#staticResources
https://micronaut-projects.github.io/micronaut-security/latest/guide/
https://micronaut-projects.github.io/micronaut-security/latest/guide/configurationreference.html

https://angular.io/cli

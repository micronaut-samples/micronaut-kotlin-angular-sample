import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainNavComponent} from './main-nav/main-nav.component';
import {LayoutModule} from '@angular/cdk/layout';
import {MaterialModule} from '../material/material.module';
import {NavRoutingModule} from './nav-routing.module';


@NgModule({
  declarations: [MainNavComponent],
  exports: [
    MainNavComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    MaterialModule,
    NavRoutingModule
  ]
})
export class NavModule {
}

FROM oracle/graalvm-ce:20.0.0-java8 as graalvm
# For JDK 11
#FROM oracle/graalvm-ce:20.0.0-java11 as graalvm
RUN gu install native-image

COPY . /home/app/spa-sample
WORKDIR /home/app/spa-sample

RUN native-image --no-server -cp build/libs/spa-sample-*-all.jar

FROM frolvlad/alpine-glibc
RUN apk update && apk add libstdc++
EXPOSE 8080
COPY --from=graalvm /home/app/spa-sample/spa-sample /app/spa-sample
ENTRYPOINT ["/app/spa-sample"]
